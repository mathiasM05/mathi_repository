package iam.mquiroga.agenda;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class Agenda extends Hashtable<String, Persona> {
	Hashtable<String, Persona> agenda;

	public Agenda(Hashtable<String, Persona> agenda) {
		this.agenda = agenda;
	}

	public Persona agrgarContacto(Persona persona){
	
		if(agenda.containsKey(persona.getNif())){
			
			Persona p =  agenda.get(persona.getNif());
			p.setTelefono(persona.getTelefono());
			return p;
		}
		else{
			agenda.put(persona.getNif(), persona);
		}
		
		return persona;
	}
	
	public boolean borrarPersona (Persona persona){
		Persona p =  agenda.get(persona.getNif());
		//Comprobamos que la persona introducida exista, es decir que tenga el Nif, nombre y telefono indicados por el usuario.
		if(agenda.containsKey(persona.getNif()))
			if(p.getNomCompleto().equals(persona.getNomCompleto()) && p.getTelefono().equals(persona.getTelefono())){
				agenda.remove(persona.getNif());
				return true;
			}
		return false;
	}
	
	public Persona buscaPersonaNif (String nif){
		if(agenda.containsKey(nif)){
			Persona p = agenda.get(nif);
			return p;
		}
		return null;
	}
	
	public Persona buscaPersonaNombre (String nom){
		Set<String> keys = agenda.keySet();
		Iterator<String> iterator = keys.iterator();
		//key = nif
		String key = null;
		while (iterator.hasNext()) { 
			key = iterator.next();
			Persona p = agenda.get(key);
			if(p.getNomCompleto().equals(nom))
				return p;
		}
		return null;
	}
	
	
}
