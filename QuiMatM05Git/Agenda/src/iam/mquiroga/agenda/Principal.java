package iam.mquiroga.agenda;

import java.util.Hashtable;
import java.util.Scanner;

public class Principal {
	
	public static void main(String[] args) {
		Hashtable<String, Persona> tablaAgenda= new Hashtable<String, Persona>();
		Agenda agenda= new Agenda(tablaAgenda);
		
		Scanner scanner= new Scanner(System.in);
		
//		Persona persona1= new Persona("Mathias Quiroga", "123", "691740439");
//		tablaAgenda.put(persona1.getNif(), persona1);
//		
//		Persona persona2= new Persona("Santiago Quiroga", "1234", "695540439");
//		tablaAgenda.put(persona2.getNif(), persona2);
		
		int opcion;
		//MENU
		do{
			System.out.println("***MENU***");
			System.out.println("1)Agregar nuevo contacto");
			System.out.println("2)Borrar contacto");
			System.out.println("3)Buscar contacto por NIF");
			System.out.println("4)Buscar contacto por nombre");
			System.out.println("5)Listar agenda");
			System.out.println("6)Salir");
			
			System.out.print("\nOpcion: ");
			opcion = scanner.nextInt();
			scanner.nextLine();
			
			if(opcion < 1 || opcion > 6)
				System.out.println("Opcion incorrecta\n");
			
			switch(opcion){
				case 1:
					System.out.print("Nombre completo: ");
					String nomCompleto= scanner.nextLine();
					
					System.out.print("NIF: ");
					String nif= scanner.next();
				
					System.out.print("Tel: ");
					String telefono= scanner.next();
					
					Persona persona= new Persona(nomCompleto, nif, telefono);
					System.out.println(agenda.agrgarContacto(persona));
					break;
				case 2:
					System.out.print("Nombre completo: ");
					nomCompleto= scanner.nextLine();
				
					System.out.print("NIF: ");
					nif= scanner.next();
				
					System.out.print("Tel: ");
					telefono= scanner.next();
					
					persona= new Persona(nomCompleto, nif, telefono);
					System.out.println(agenda.borrarPersona(persona));
					break;
					
				case 3:
					System.out.print("NIF: ");
					nif= scanner.next();
					System.out.println(agenda.buscaPersonaNif(nif));
					break;
				case 4:
					System.out.print("Nombre completo: ");
					nomCompleto= scanner.nextLine();
					System.out.println(agenda.buscaPersonaNombre(nomCompleto));
					break;
				case 5:
					System.out.println(tablaAgenda.toString());
					break;
				case 6:
					System.out.println("Programa cerrado");
					break;
			}
		}while(opcion != 6);
	}
}
