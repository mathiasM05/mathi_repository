package iam.mquiroga.agenda;

public class Persona {
	String nomCompleto, nif, telefono;

	public Persona(String nomCompleto, String dni, String telefono) {
		this.nomCompleto = nomCompleto;
		this.nif = dni;
		this.telefono = telefono;
	}
	
	public Persona (){
		
	}
	
	public String getNomCompleto() {
		return nomCompleto;
	}

	public void setNomCompleto(String nomCompleto) {
		this.nomCompleto = nomCompleto;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String dni) {
		this.nif = dni;
	}
	
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Override
	public String toString() {
		return "Persona [nomCompleto= " + nomCompleto + ", nif =" + nif
				+ ", telefono =" + telefono + "]\n";
	}
	
	
}
